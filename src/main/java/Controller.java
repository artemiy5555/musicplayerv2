import dao.SongDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.Artist;
import model.Genre;
import model.Song;

import java.io.File;
import java.util.List;

public class Controller {

    private ObservableList<String> genge = FXCollections.observableArrayList();
    private ObservableList<String> artist = FXCollections.observableArrayList();

    @FXML private ComboBox<String> comboGenre = new ComboBox<>();
    @FXML private ComboBox<String> comboArtist = new ComboBox<>();

    @FXML private TableView<Song> table = new TableView<>();
    @FXML private TableColumn<Song, String> columSong;
    @FXML private TableColumn<Song, String> columArtist;
    @FXML private TableColumn<Song, String> columGenge;
    @FXML private TableColumn<Song, String> columAlbum;
    @FXML private TableColumn<Song, String> columYear;
    @FXML private TableColumn<Song, String> columTime;

    private MediaPlayer player;

    public void initialize() {
        columSong.setCellValueFactory(new PropertyValueFactory<>("name"));
        columArtist.setCellValueFactory(new PropertyValueFactory<>("artist"));
        columGenge.setCellValueFactory(new PropertyValueFactory<>("genre"));
        columAlbum.setCellValueFactory(new PropertyValueFactory<>("album"));
        columYear.setCellValueFactory(new PropertyValueFactory<>("year"));
        columTime.setCellValueFactory(new PropertyValueFactory<>("time"));

        table.setRowFactory(tv -> {
            TableRow<Song> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    String uriString = new File(table.getSelectionModel().getSelectedItem().getLink().replaceAll("\\?", "")).toURI().toString();
                     try {
                        stop();
                        player = null;
                    } catch (Exception ignored) {}
                    player = new MediaPlayer(new Media(uriString));
                    play();
                }
            });
            return row;
        });

    }

    @FXML
    private void open() {

//        final DirectoryChooser directoryChooser = new DirectoryChooser();
//
//        File dir = directoryChooser.showDialog(null);
//        String str ="\\\\";
//        SongDao.setLink(String.valueOf(dir).replaceAll(str,"/"));

        List<Genre> genres = SongDao.getGenre();
        for (Genre genre : genres) {

            genge.add(genre.getGenre());
        }
        comboGenre.setItems(genge);
    }

    @FXML
    private void actionComboGenre() {
        artist.clear();
        List<Artist> artists = SongDao.getArtistsByGenre(comboGenre.getValue());
        for (Artist artist : artists) {
            this.artist.add(artist.getArtists());
        }

        artist.addAll();
        comboArtist.setItems(artist);
    }

    @FXML
    private void actionComboArtist() {
        ObservableList<Song> songData = FXCollections.observableArrayList();
        songData.addAll(SongDao.getSongByArtist(comboArtist.getValue()));
        table.setItems(songData);
    }

    @FXML
    private void play() {
        player.play();
        System.out.println();
    }

    @FXML
    private void stop() {
        player.pause();
    }

}
