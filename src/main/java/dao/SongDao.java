package dao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import model.Artist;
import model.Genre;
import model.Song;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;

public class SongDao {

    public static void setLink(String path) {
        try {
            String url = "http://localhost:8080/musicServlet_war/getLink?link=" + path;
            System.out.println(url);
            Jsoup.connect(url).ignoreContentType(true).get();

        } catch (Exception ex) {
            System.err.println(ex);

        }
    }

    public static List getGenre() {
        List<Genre> genres = new ArrayList<Genre>();
        try {
            String url = "http://localhost:8080/musicServlet_war/getGenre";
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());
            genres = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Genre>>() {
                    }.getType());

        } catch (Exception ex) {
            System.err.println(ex);

        }

        return genres;
    }

    public static List getArtistsByGenre(String genre) {
        List<Artist> artists = new ArrayList<Artist>();
        try {
            String url = "http://localhost:8080/musicServlet_war/getArtists?genre=" + genre;
            System.out.println(url);
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());

            artists = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Artist>>() {
                    }.getType());

        } catch (Exception ex) {
            System.err.println(ex);

        }
        return artists;

    }

    public static List getSongByArtist(String genre) {
        List<Song> songs = new ArrayList<>();
        try {
            String url = "http://localhost:8080/musicServlet_war/getSong?artist=" + genre;
            System.out.println(url);
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());


            songs = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Song>>() {
                    }.getType());

        } catch (Exception ex) {
            System.err.println(ex);

        }

        return songs;
    }
}
